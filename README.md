# 🔒 Project Archived 🔒

The content of this repository was moved inside [salsa-ci-team/pipeline](https://salsa.debian.org/salsa-ci-team/pipeline).



# atomic reprotest

This is a tool to help debugging failures reported by reprotest.

To use it, you need to run an atomic-reprotest pipeline, following these
steps:

  1. Fork atomic-reprotest to a salsa namespace where you have rights to
     run pipelines.
  1. Run a pipeline passing these variables:
      * `TARGET_GIT_URL`: GIT URL to clone the project to debug.
      * `TARGET_GIT_BRANCH`: branch name (optional, default: master)

And that's it!
